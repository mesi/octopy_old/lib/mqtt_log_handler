import logging
import paho.mqtt.client as mqtt
import json
import traceback
import time

DEFAULT_LOG_TOPIC = 'octopy/log'
MQTT_QOS = 1

class MQTTLogHandler(logging.Handler):
    """ Logging over MQTT.
    Takes a paho.mqtt.client-object and a base topic to publish on."""
    def __init__(self, host, port, username, password, log_topic = DEFAULT_LOG_TOPIC):
        logging.Handler.__init__(self)
        self._host = host
        self._port = port
        self._username = username
        self._password = password
        self._log_topic = str(log_topic)
        self.start()


    def start(self):
        # Setup MQTT connection
        self._mqtt_client = mqtt.Client()
        if self._username and self._password:
            self._mqtt_client.username_pw_set(self._username, password=self._password)
        self._mqtt_client.connect(self._host, self._port)
        # Start MQTT loop thread
        self._mqtt_client.loop_start()


    def emit(self, record):
        if not self._mqtt_client:
            return
        message = {
            "message": record.message,
            "source": record.name,
            "process": record.process,
            "timestamp": time.time(),
            "stack": []
        }
        stack = traceback.extract_stack()[:-6]  # Hack - the last 6 calls are for the the actual logging call and we don't want to include them.
        for frame in stack:
            message['stack'].append( {
                "filename": frame.filename,
                "lineno": frame.lineno,
                "name": frame.name
            } )
        jsonmsg = json.dumps(message)
        try:
            self._mqtt_client.publish(self._log_topic + '/' + record.levelname.lower(), jsonmsg, qos = MQTT_QOS)
        except BaseException as e:
            print(f"Error on publishing log message to MQTT: {e}")

